let ul = document.querySelector('.tabs');
let activeClass = document.querySelector('.active');
let text = document.querySelector('.tabs-content');
invisibleTextCreatAtribute();
visible(activeClass);
ul.addEventListener('click', (event) => {
    visible(event.target);
    if (activeClass) {
        activeClass.classList.toggle('active');
    }
    event.target.classList.toggle('active');
    activeClass = event.target;
});
function invisibleTextCreatAtribute() {
    let i = 0;
    let li = ul.querySelectorAll('li');
    for (let p of text.querySelectorAll('li')) {
        let atrbute = document.createAttribute('data-name');
        if (li[i] === undefined) {
            console.log("There is a text that is not available to the user and must be added to the menu");
        } else {
            atrbute.value = li[i].innerText;
            p.setAttributeNode(atrbute);
            i++;
        };
    };
};
function visible(target) {
    for (let p of text.querySelectorAll('li')) {
        p.hidden = p.dataset.name !== target.innerText;
        
    }
}